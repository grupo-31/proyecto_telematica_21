import requests
import json

API_ENDPOINT = "https://apibenchmark.herokuapp.com/"
amount = 10
data = {
  "url": "https://www.reddit.com/",
  "method": "GET",
  "usersAmount": amount,
  "requestsPerUser": 1,
  "body": ""
}
r = requests.post(url = API_ENDPOINT, data = data)
errors = json.loads(r.text)["result"]["errors"]
if errors == 0 :
    print("Test Passed")
else:
    raise Exception('The test failed with ' + str(errors) + ' of ' + str(amount))
